﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

	public AudioSource[] Sounds;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void PlayButtonClick()
	{
		Sounds [0].Play ();
	}

	public void PlaySwoosh()
	{
		Sounds [1].Play ();
	}

	public void PlayHeat()
	{
		Sounds [2].Play ();
	}

	public void PlayExp()
	{
		Sounds [3].Play ();
	}

	public void PlayReady()
	{
		Sounds [4].Play ();
		//Sounds [5].PlayDelayed (0.4f);
		//Sounds [5].loop = true;
	}

	public void PlayMenuChange ()
	{
		Sounds [6].PlayDelayed (0.25f);
	}


	public void PlayBeep ()
	{
		Sounds [7].Play ();
	}
}
