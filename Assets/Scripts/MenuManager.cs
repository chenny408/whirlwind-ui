﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

	public WhirlWindController _whirlWindController; // instance of the WhirlWind Plugin
	public menu CurrentMenu; // menu control
	public AudioManager am; // audio manager

	public Dropdown fanLevelDropDown;
	public Dropdown heatLevelDropDown;
	public Dropdown burstCountDropDown;
	public Slider FlowControl;
	public Slider DamperControl;
	public Button[] EffectButtons;
	public Button CloseButton;
	public Button HapticsMenuButton;
	public Text flowValue;
	public Text damperValue;
	public Text SystemReadyText;
	public Toggle heatToggle;

	public GameObject[] particles;
	float currDamperValue = 0;
	float currFlowValue = 0;
	bool ThresholdValueHit = false;
	bool SystemReady = false;
	private bool displayMainTitleLabel = false;

	public float DefaultFlowEffectDuration = 10; //seconds
	public float DefaultHeatEffectDuration = 10; //seconds
	public float DefaultExplosionAfterFlowDuration = 0.0f; //seconds
	public int MAX_HEATER_ON_LIMIT = 2400; //40 mins in seconds

	public int rev = 3; // default value for multiple bursts

	void Awake()
	{
		foreach (GameObject g in particles) 
		{
			g.SetActive (false);

		}
	}

	// Use this for initialization
	void Start () 
	{
		StartCoroutine (FlashMainTitle ());
		ShowMenu (CurrentMenu);

		currDamperValue = (int)DefaultParameters.damperAmbient;
	}

	IEnumerator FlashMainTitle ()
	{
		while (!_whirlWindController.IsWhirlWindSystemReady ()) 
		{
			displayMainTitleLabel = true;
			yield return new WaitForSeconds(.5f);
			displayMainTitleLabel = false;
			yield return new WaitForSeconds(.5f); 
		}
	}

	public void ShowMenu(menu _menu)
	{
		am.PlayMenuChange ();

		if (_whirlWindController.IsWhirlWindSystemReady ()) 
		{
			am.PlayButtonClick ();
		}

		if (CurrentMenu != null) 
		{
			CurrentMenu.IsOpen = false;
		}

		CurrentMenu = _menu;
		CurrentMenu.IsOpen = true;

		_whirlWindController.StopAllEffects ();
	}

	public void ExitApplication()
	{
		am.PlayButtonClick ();
		if (_whirlWindController.IsWhirlWindSystemReady ()) 
		{
			_whirlWindController.StopAllEffects ();
		}

		Application.Quit ();
	}

	// Update is called once per frame
	void Update () 
	{
	
		if(_whirlWindController.IsWhirlWindSystemReady()) 
		{

			FlowControlFanUpdate ();
			DamperControlServoUpdate ();

		}
	}

	void OnGUI()
	{
		UpdatePanelTexts ();

		float flowPWMSliderValue = FlowControl.value;
		if (flowPWMSliderValue >= 20) 
		{
			if(ThresholdValueHit==false)
			{
				ThresholdValueHit = true;
			}
		}
			
	}

	void UpdatePanelTexts()
	{
		if (_whirlWindController.IsWhirlWindSystemReady ()) 
		{

			FlowControl.interactable = true;
			DamperControl.interactable = true;
			HapticsMenuButton.interactable = true;

			CurrentMenu.SetMenuInteractive (true);

			SystemReadyText.enabled = true;
			SystemReadyText.color = Color.white;
			flowValue.text = ((int)FlowControl.value).ToString ();

			if (!SystemReady) 
			{
				am.PlayReady ();
				SystemReady = true;
				SystemReadyText.text = "BENDING AIR AT "+_whirlWindController.GetCOMPortName ();
			}

			foreach (GameObject g in particles) 
			{
				g.SetActive (true);
			}

			if ((int)DamperControl.value == 0)
				damperValue.text = "A";
			else
				damperValue.text = "H";
		} 
		else 
		{
			FlowControl.interactable = false; 
			DamperControl.interactable = false;
			HapticsMenuButton.interactable = false;


			if (displayMainTitleLabel == true)
				SystemReadyText.enabled = true;
			else
				SystemReadyText.enabled = false;
		}

	}

	// callback function for flow effect button
	public void FlowActivate()
	{
		
		am.PlaySwoosh ();
		//_whirlWindController.StopAllEffects ();

		int flevel = GetFanLevelDropDownvalue ();

		Debug.Log ("Triggering air flow: level = " + flevel);
		_whirlWindController.Flow (flevel, DefaultFlowEffectDuration);
	}

	// callback function for heat effect button
	public void HeatActivate()
	{
		am.PlayHeat ();
		//_whirlWindController.StopAllEffects ();

		int hlevel = GetHeatLevelDropDownValue ();
		int hvalue = (int)HeatLevel.heat1;

		Debug.Log ("Triggering heat: level = " + hlevel + ", power (%) = " + hvalue);
		_whirlWindController.HeatedFlow (hlevel, hvalue, (int)DefaultParameters.defaultFan, DefaultHeatEffectDuration);
	}

	// callback function for explosion button
	public void ExplosionActivate()
	{
		am.PlayExp ();

		int count = GetExplosionDropDownvalue ();
		int fvalue = (int)FanLevel.fan5;
		int hvalue = (int)HeatLevel.heat0; //no heat
		int hctrl = 0; //0% intensity

		Debug.Log ("Triggering burst: count = " + count + ", fan value = " + fvalue);
		_whirlWindController.ExplosionBurst (count,fvalue,hvalue,hctrl);

		StartCoroutine (ButtonTimer (EffectButtons [2], 0.4f));
	}
		
	IEnumerator ButtonTimer(Button b, float duration)
	{
		b.interactable = false;
		yield return new WaitForSeconds (duration);
		b.interactable = true;
	}

	int GetHeatLevelDropDownValue()
	{
		switch (heatLevelDropDown.value) 
		{
		case 0: 
			return (int)HeatLevel.heat0;//break;
		case 1:
			return (int)HeatLevel.heat1;//break;
		case 2:
			return (int)HeatLevel.heat2;//break;
		default:
			return (int)HeatLevel.heat0;//break;
		}
	}

	int GetFanLevelDropDownvalue()
	{
		switch (fanLevelDropDown.value) 
		{
		case 0: 
			return (int)FanLevel.fan0;//break;
		case 1:
			return (int)FanLevel.fan1;//break;
		case 2:
			return (int)FanLevel.fan3;//break;
		case 3:
			return (int)FanLevel.fan5;//break;
		default:
			return (int)FanLevel.fan0;//break;

		}
	}

	int GetExplosionDropDownvalue()
	{
		switch (burstCountDropDown.value) 
		{
		case 0: 
			return 1;//break;
		case 1:
			return rev;//break;
		default:
			return 0;//break;

		}
	}

	// updates the fan level based on the slider control for the fan
	void FlowControlFanUpdate()
	{
		//Flow Control
		float flowPWMSliderValue = FlowControl.value;
		if (flowPWMSliderValue != currFlowValue) 
		{
			if (ThresholdValueHit) 
			{
				_whirlWindController.FanMotorWrite ((int)flowPWMSliderValue);
				//arduino.analogWrite((int)Pins.FanPin, (int)flowPWMSliderValue);
				currFlowValue = flowPWMSliderValue;
			}

		}
	}

	int calculate_servo_pos(float servo_slider_value)
	{
		float servo_level = servo_slider_value / 100f; 
		float servo_offset = (DefaultParameters.damperHeat - DefaultParameters.damperAmbient) * servo_level;
		int servo_pos = (int)DefaultParameters.damperAmbient + (int)servo_offset; //offset could be negative
		Debug.Log ("servo: level = " + servo_level + ", offset = " + servo_offset + ", pos = " + servo_pos);
		return (int) servo_pos;
	}

	// Updates the damper angle based on the slider control for the damper
	void DamperControlServoUpdate()
	{
		//Damper Control
		float damperServoSliderValue = calculate_servo_pos(DamperControl.value);

		//reverse the range as the angles are now reversed
		//damperServoSliderValue = -1 * damperServoSliderValue + 282;

		if (damperServoSliderValue != currDamperValue) 
		{
			Debug.Log ("servo value = " + damperServoSliderValue);

			if (true) 
			{
				/*
				if (damperServoSliderValue == 0) 
				{
					_whirlWindController.DamperServoWrite ((int)DefaultParameters.damperAmbient);
					//arduino.analogWrite((int)Pins.damperPin, (int)damperAmbient);
					currDamperValue = damperServoSliderValue;
				}
				else if(damperServoSliderValue == 1)
				{
					_whirlWindController.DamperServoWrite ((int)DefaultParameters.damperHeat);
					//arduino.analogWrite((int)Pins.damperPin, (int)damperHeat);
					currDamperValue = damperServoSliderValue;
				}
				*/

				_whirlWindController.DamperServoWrite ((int)damperServoSliderValue);
				currDamperValue = damperServoSliderValue;

			}

		}
	}

	void OnApplicationQuit()
	{
		am.PlayButtonClick ();
		if (_whirlWindController.IsWhirlWindSystemReady ()) 
		{
			_whirlWindController.StopAllEffects ();
		}
	}

	// Toggles the heating state to on or off based on the UI toggle button
	public void ToggleHeating()
	{
		if (heatToggle.isOn) {
			Debug.Log ("Triggering heat on..");
			_whirlWindController.SetHeaters (1); //turn on
			StartCoroutine(triggerDuration(MAX_HEATER_ON_LIMIT)); //40 second heater on limit
		} else {
			Debug.Log ("Triggering heat off..");
			_whirlWindController.SetHeaters (0); //turn off
		}
	}

	IEnumerator triggerDuration(int sec)
	{
		yield return new WaitForSeconds (sec);
		heatToggle.isOn = false;
		_whirlWindController.StopAllEffects();
	}
}
