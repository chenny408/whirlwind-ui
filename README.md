# README for Vortx Utility Software #

UNITY VERSION USED: Unity Pro 5.4.1f1 (64 bit)

There may be some issues with the Standard Asset (ImageEffects/DepthOfField) with version 5.3 and earlier.

* Quick summary

This project provides an UI with test controls for the Vortx device.  The UI allows for adjusting air flow speeds, heat settings, and triggering bursts.  The UI is primarily used for debugging the Vortx hardware, however the source code is also useful as a demo application for device integration.

* Notes

Vortx is designed to automatically shut off its heater after 40 minutes of activity to prevent issues with overheating.

The Flow and Heat effects under Haptic Effects are designed to last for 10 seconds.